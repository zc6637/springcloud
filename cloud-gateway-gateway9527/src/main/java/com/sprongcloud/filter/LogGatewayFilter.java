package com.sprongcloud.filter;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Date;

/**
 * description: GatewayFilter 全局过滤器 <br>
 * date: 2020/5/27 9:21 <br>
 * author: 11752 <br>
 * version: 1.0 <br>
 */
@Slf4j
public class LogGatewayFilter implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("***********come in GatewayFilter:  " + new Date());

        /*String username = exchange.getRequest().getQueryParams().getFirst("username");

        if (!"zhangsan".equals(username)) {
            log.info("*******非法用户");
            exchange.getResponse().setStatusCode(HttpStatus.NOT_ACCEPTABLE);
            return exchange.getResponse().setComplete();
        }*/
        return chain.filter(exchange);
    }

    /**
     * 多个过滤器排序执行，返回得值越小执行优先级就越大
     *
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
