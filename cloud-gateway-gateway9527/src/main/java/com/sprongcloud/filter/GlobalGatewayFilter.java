package com.sprongcloud.filter;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * description: DataGatewayFilter 全局过滤器配置 <br>
 * date: 2020/5/27 9:48 <br>
 * author: 11752 <br>
 * version: 1.0 <br>
 */
@Configuration
public class GlobalGatewayFilter {

    @Bean
    public GlobalFilter customFilter() {
        return new CustomGlobalFilter();
    }

    @Bean
    public GlobalFilter logFilter() {
        return new LogGatewayFilter();
    }

}
