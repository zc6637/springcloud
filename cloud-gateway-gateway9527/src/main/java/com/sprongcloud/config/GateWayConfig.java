package com.sprongcloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GateWayConfig {

    @Bean
    public RouteLocator routes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("circuitbreaker_route", r -> r.path("/guonei")
                        .uri("http://news.baidu.com/guonei"))
                .route("circuitbreaker_route1", r -> r.path("/guoji")
                        .uri("http://news.baidu.com/guoji"))
                .build();
    }

}
