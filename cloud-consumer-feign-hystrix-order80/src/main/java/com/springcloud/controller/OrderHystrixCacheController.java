package com.springcloud.controller;

import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import com.springcloud.entities.CommonResult;
import com.springcloud.service.cache.PaymentHystrixRequestCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
public class OrderHystrixCacheController {

    @Resource
    private PaymentHystrixRequestCacheService paymentHystrixRequestCacheService;


    /**
     * Hystrix 请求缓存
     * @param id
     * @return
     */
    @GetMapping("/testCache/{id}")
    public CommonResult testCache(@PathVariable("id") Long id) {
        CommonResult userCache = paymentHystrixRequestCacheService.getUserCache(id);
        CommonResult userCache1 = paymentHystrixRequestCacheService.getUserCache(id);
        CommonResult userCache2 = paymentHystrixRequestCacheService.getUserCache(id+2);
        return new CommonResult(200,userCache.getMessage() + userCache1.getMessage()+ userCache2.getMessage());
    }

}
