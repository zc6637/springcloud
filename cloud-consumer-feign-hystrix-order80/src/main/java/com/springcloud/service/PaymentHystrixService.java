package com.springcloud.service;

import com.netflix.hystrix.contrib.javanica.cache.annotation.CacheResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT",fallback = PaymentFallbackServiceImpl.class)
public interface PaymentHystrixService {

    @GetMapping("/payment/hystrix/ok/{id}")
    String paymentInfo_OK(@PathVariable("id") Integer id);

    @GetMapping("/payment/hystrix/timeout/{id}")
    String paymentInfo_TimeOut(@PathVariable("id") Integer id);

    @GetMapping("/payment/testCache/{id}")
    @CacheResult(cacheKeyMethod = "getCacheKey")
    // @HystrixCommand(fallbackMethod = "getDefaultUser", commandKey = "testCache")
    String testCache(@PathVariable("id") Long id);

}
