package com.springcloud.controller;

import com.springcloud.pojo.GitConfigData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope//刷新配置
@RestController
public class ConfigClientController {

    @Autowired
    private GitConfigData gitConfigData;

    @GetMapping("/configInfo")
    public GitConfigData getConfigInfo() {
        return gitConfigData;
    }
}