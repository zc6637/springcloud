package com.springcloud.pojo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author 11752
 * @version 1.0
 * @date 2020/6/4 16:54
 */
@Data
@Component
@ConfigurationProperties(prefix = "data")
public class GitConfigData {

    private String port;

    private String name;

    private String info;

}